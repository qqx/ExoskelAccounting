import logging
import sys

log = logging.getLogger(__name__)
handler = logging.StreamHandler(stream=sys.stdout)
formatter = logging.Formatter(fmt=logging.BASIC_FORMAT)
handler.setFormatter(formatter)
log.addHandler(handler)
log.setLevel(logging.DEBUG)


class Accountant:
    """Will eventually count 1 for each hit to an endpoint.
    """

    def __init__(self, application):
        log.info("Initialize Accountant(%s)", application)
        self.downstream_app = application

    def __call__(self, upstream_env, upstream_start_response):
        """Called by the upstream server/middleware.
        Manufactures a downstream_start_response function for downstream (which returns
        a downstream_write function which we might manufacture or we can use the one we
        get when we call the upstream_start_response function.).
        Manufactures/modifies and env to send downstream.
        Call the upstream_start_response.
        Call the downstream_app with downstream_env, downstream_start_response
        Returns an iterable.
        """
        downstream_env = upstream_env

        def downstream_writer(some_bytes):  # actually not used.
            print(str(some_bytes))

        def downstream_start_response(status, headers, exc_info=None):
            log.debug("Middleware status: {}".format(status))
            log.debug("Middleware headers: {}".format(headers))
            # When our downstream calls this func we can get hold of the actual upstream_writer from our upstream
            upstream_writer = upstream_start_response(status, headers)
            return upstream_writer

        downstream_iter = self.downstream_app(downstream_env,
                                              downstream_start_response)

        my_iter = downstream_iter
        return my_iter


def inner_app(env, start_response):
    start_response("200 OK", [('Content-type', 'text/plain')])
    return ["hello world"]


def test_test():
    # make the middleware
    accountant_middleware = Accountant(inner_app)
    # make a request
    env = {}

    def downstream_writer(some_bytes):
        print(str(some_bytes))

    def fake_start_response(status, headers):
        downstream_writer("{}".format(status))
        for header_name, header_value in headers:
            downstream_writer("{}: {}".format(header_name, header_value))
        downstream_writer("")
        return downstream_writer

    for d in accountant_middleware(env, fake_start_response):
        print(d)
