from setuptools import setup
setup(
    name="ExoskelAccounting",
    version="0.1",
    package_dir={'': 'src'},
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
